package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSearch {

    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("cat_id")
    @Expose
    private Integer catId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("price_ind")
    @Expose
    private Double priceInd;
    @SerializedName("price_mosque")
    @Expose
    private Double priceMosque;
    @SerializedName("price_corp")
    @Expose
    private Double priceCorp;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getPriceInd() {
        return priceInd;
    }

    public void setPriceInd(Double priceInd) {
        this.priceInd = priceInd;
    }

    public Double getPriceMosque() {
        return priceMosque;
    }

    public void setPriceMosque(Double priceMosque) {
        this.priceMosque = priceMosque;
    }

    public Double getPriceCorp() {
        return priceCorp;
    }

    public void setPriceCorp(Double priceCorp) {
        this.priceCorp = priceCorp;
    }

}