package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmergencyRequestResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("emergency_request_search")
    @Expose
    private List<EmergencyRequestSearch> emergencyRequestSearch = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<EmergencyRequestSearch> getEmergencyRequestSearch() {
        return emergencyRequestSearch;
    }

    public void setEmergencyRequestSearch(List<EmergencyRequestSearch> emergencyRequestSearch) {
        this.emergencyRequestSearch = emergencyRequestSearch;
    }

}