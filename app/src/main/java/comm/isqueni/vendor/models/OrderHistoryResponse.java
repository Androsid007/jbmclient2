package comm.isqueni.vendor.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderHistoryResponse {

@SerializedName("status")
@Expose
private Integer status;
@SerializedName("order_history")
@Expose
private List<OrderHistory> orderHistory = null;

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public List<OrderHistory> getOrderHistory() {
return orderHistory;
}

public void setOrderHistory(List<OrderHistory> orderHistory) {
this.orderHistory = orderHistory;
}

}