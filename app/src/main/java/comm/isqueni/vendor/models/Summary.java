package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Summary {

    @SerializedName("new_order")
    @Expose
    private Integer newOrder;
    @SerializedName("new_risk")
    @Expose
    private Integer newRisk;
    @SerializedName("all_product")
    @Expose
    private Integer allProduct;
    @SerializedName("all_active_driver")
    @Expose
    private Integer allActiveDriver;

    public Integer getNewOrder() {
        return newOrder;
    }

    public void setNewOrder(Integer newOrder) {
        this.newOrder = newOrder;
    }

    public Integer getNewRisk() {
        return newRisk;
    }

    public void setNewRisk(Integer newRisk) {
        this.newRisk = newRisk;
    }

    public Integer getAllProduct() {
        return allProduct;
    }

    public void setAllProduct(Integer allProduct) {
        this.allProduct = allProduct;
    }

    public Integer getAllActiveDriver() {
        return allActiveDriver;
    }

    public void setAllActiveDriver(Integer allActiveDriver) {
        this.allActiveDriver = allActiveDriver;
    }

}