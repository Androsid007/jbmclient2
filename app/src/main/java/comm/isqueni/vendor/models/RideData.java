package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 12/7/2017.
 */

public class RideData{

    @SerializedName("notify_type")
    @Expose
    private String notifyType;
    @SerializedName("notify_data")
    @Expose
    private String notifyData;
    @SerializedName("order_id")
    @Expose
    private String orderId;

    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    public String getNotifyData() {
        return notifyData;
    }

    public void setNotifyData(String notifyData) {
        this.notifyData = notifyData;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

}


