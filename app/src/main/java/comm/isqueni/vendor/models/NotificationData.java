package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 12/7/2017.
 */

public class NotificationData {

    @SerializedName("ride_data")
    @Expose
    private String rideData;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("title")
    @Expose
    private String title;

    public String getRideData() {
        return rideData;
    }

    public void setRideData(String rideData) {
        this.rideData = rideData;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}


