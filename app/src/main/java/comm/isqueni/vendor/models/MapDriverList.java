package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MapDriverList {

@SerializedName("driver_id")
@Expose
private Integer driverId;
@SerializedName("vendor_id")
@Expose
private String vendorId;
@SerializedName("longitude")
@Expose
private String longitude;
@SerializedName("latitude")
@Expose
private String latitude;
@SerializedName("name")
@Expose
private String name;
@SerializedName("mobile")
@Expose
private String mobile;
@SerializedName("car_type")
@Expose
private String carType;
@SerializedName("car_number")
@Expose
private String carNumber;
@SerializedName("driver_licence_number")
@Expose
private String driverLicenceNumber;
@SerializedName("image")
@Expose
private String image;

public Integer getDriverId() {
return driverId;
}

public void setDriverId(Integer driverId) {
this.driverId = driverId;
}

public String getVendorId() {
return vendorId;
}

public void setVendorId(String vendorId) {
this.vendorId = vendorId;
}

public String getLongitude() {
return longitude;
}

public void setLongitude(String longitude) {
this.longitude = longitude;
}

public String getLatitude() {
return latitude;
}

public void setLatitude(String latitude) {
this.latitude = latitude;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getMobile() {
return mobile;
}

public void setMobile(String mobile) {
this.mobile = mobile;
}

public String getCarType() {
return carType;
}

public void setCarType(String carType) {
this.carType = carType;
}

public String getCarNumber() {
return carNumber;
}

public void setCarNumber(String carNumber) {
this.carNumber = carNumber;
}

public String getDriverLicenceNumber() {
return driverLicenceNumber;
}

public void setDriverLicenceNumber(String driverLicenceNumber) {
this.driverLicenceNumber = driverLicenceNumber;
}

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

}