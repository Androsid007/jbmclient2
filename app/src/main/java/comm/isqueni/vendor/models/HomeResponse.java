package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeResponse {

@SerializedName("status")
@Expose
private Integer status;
@SerializedName("Summary")
@Expose
private Summary summary;

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public Summary getSummary() {
return summary;
}

public void setSummary(Summary summary) {
this.summary = summary;
}

}