package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmergencyRequestSearch {

    @SerializedName("request_id")
    @Expose
    private Integer requestId;
    @SerializedName("action")
    @Expose
    private Integer action;
    @SerializedName("vendor_id")
    @Expose
    private Integer vendorId;
    @SerializedName("driver_id")
    @Expose
    private Integer driverId;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("emergeny_date")
    @Expose
    private String emergenyDate;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("order_id")
    @Expose
    private String order_id;

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getEmergenyDate() {
        return emergenyDate;
    }

    public void setEmergenyDate(String emergenyDate) {
        this.emergenyDate = emergenyDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}