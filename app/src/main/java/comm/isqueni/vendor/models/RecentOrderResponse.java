package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecentOrderResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("order_placed")
    @Expose
    private List<RecentOrderList> orderList = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<RecentOrderList> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<RecentOrderList> orderList) {
        this.orderList = orderList;
    }
}
