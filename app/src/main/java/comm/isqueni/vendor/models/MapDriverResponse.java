package comm.isqueni.vendor.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MapDriverResponse {

@SerializedName("status")
@Expose
private Integer status;
@SerializedName("driver_search")
@Expose
private List<MapDriverList> driverSearch = null;

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public List<MapDriverList> getDriverSearch() {
return driverSearch;
}

public void setDriverSearch(List<MapDriverList> driverSearch) {
this.driverSearch = driverSearch;
}

}