package comm.isqueni.vendor.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductListResponse {

    @SerializedName("status")
    @Expose
    private Integer status;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    @SerializedName("Message")
    @Expose
    private String Message;
    @SerializedName("resObj")
    @Expose
    private List<ProductSearch> productSearch = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<ProductSearch> getProductSearch() {
        return productSearch;
    }

    public void setProductSearch(List<ProductSearch> productSearch) {
        this.productSearch = productSearch;
    }

}