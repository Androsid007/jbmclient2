package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderHistory {

    @SerializedName("isDriverAlreadyAssign")
    @Expose
    private Integer isDriverAlreadyAssign;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("driver_mobile")
    @Expose
    private Object driverMobile;
    @SerializedName("invoice_numder")
    @Expose
    private String invoiceNumder;
    @SerializedName("order_status")
    @Expose
    private Integer orderStatus;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_price")
    @Expose
    private Double productPrice;
    @SerializedName("qty")
    @Expose
    private Double qty;
    @SerializedName("coupon_code")
    @Expose
    private String couponCode;
    @SerializedName("vendor_id")
    @Expose
    private Integer vendorId;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("client_id")
    @Expose
    private String clientId;
    @SerializedName("shipping_id")
    @Expose
    private String shippingId;
    @SerializedName("order_datetime")
    @Expose
    private String orderDatetime;
    @SerializedName("total_amount")
    @Expose
    private Double totalAmount;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("payment_type")
    @Expose
    private Integer paymentType;
    @SerializedName("payment_amount")
    @Expose
    private Integer paymentAmount;
    @SerializedName("shipping_charge")
    @Expose
    private Integer shippingCharge;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("client_name")
    @Expose
    private String clientName;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("house")
    @Expose
    private String house;
    @SerializedName("landmark")
    @Expose
    private String landmark;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getIsDriverAlreadyAssign() {
        return isDriverAlreadyAssign;
    }

    public void setIsDriverAlreadyAssign(Integer isDriverAlreadyAssign) {
        this.isDriverAlreadyAssign = isDriverAlreadyAssign;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public Object getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(Object driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getInvoiceNumder() {
        return invoiceNumder;
    }

    public void setInvoiceNumder(String invoiceNumder) {
        this.invoiceNumder = invoiceNumder;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getShippingId() {
        return shippingId;
    }

    public void setShippingId(String shippingId) {
        this.shippingId = shippingId;
    }

    public String getOrderDatetime() {
        return orderDatetime;
    }

    public void setOrderDatetime(String orderDatetime) {
        this.orderDatetime = orderDatetime;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Integer paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Integer getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(Integer shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}