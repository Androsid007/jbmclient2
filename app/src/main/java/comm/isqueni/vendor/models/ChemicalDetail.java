package comm.isqueni.vendor.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChemicalDetail {

@SerializedName("chemical_name")
@Expose
private String chemicalName;
@SerializedName("chemical_value")
@Expose
private String chemicalValue;

public String getChemicalName() {
return chemicalName;
}

public void setChemicalName(String chemicalName) {
this.chemicalName = chemicalName;
}

public String getChemicalValue() {
return chemicalValue;
}

public void setChemicalValue(String chemicalValue) {
this.chemicalValue = chemicalValue;
}

}