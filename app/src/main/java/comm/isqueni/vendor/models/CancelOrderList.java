package comm.isqueni.vendor.models;

public class CancelOrderList {
    String mString;

    public CancelOrderList(String mString) {
        this.mString = mString;
    }

    public String getmString() {
        return mString;
    }

    public void setmString(String mString) {
        this.mString = mString;
    }
}
