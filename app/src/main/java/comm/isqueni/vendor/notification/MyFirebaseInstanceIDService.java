package comm.isqueni.vendor.notification;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import comm.isqueni.vendor.common.SharedPreferenceManager;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
   private static final String TAG = "MyFirebaseIIDService";
  @Override
    public void onTokenRefresh() {
           String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        SharedPreferenceManager.setdeviceToken(MyFirebaseInstanceIDService.this,refreshedToken);
   }
   private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.



    }
}