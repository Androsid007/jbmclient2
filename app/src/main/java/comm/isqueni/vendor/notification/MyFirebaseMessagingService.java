package comm.isqueni.vendor.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Map;

import comm.isqueni.vendor.R;
import comm.isqueni.vendor.activity.HomeActivity;
import comm.isqueni.vendor.common.NotificationUtility;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.models.NotificationData;
import comm.isqueni.vendor.models.RideData;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private Notification notification;
    private NotificationData notificationData;
    private RideData rideData;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        {

            Log.d(TAG, "From: " + remoteMessage.getFrom());


            if (remoteMessage.getData().size() > 0) {
                Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);
                Log.e("JSON_OBJECT", object.toString());
                try {
                     notificationData = new Gson().fromJson(object.toString(), NotificationData.class);
                    String data = notificationData.getRideData();
                    JSONObject object1 = new JSONObject(data);
                    if (data != null) {
                         rideData = new Gson().fromJson(object1.toString(), RideData.class);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

       Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (rideData!=null &&rideData.getNotifyType() != null && rideData.getNotifyType().equals("assign_driver")) {
                            SharedPreferenceManager.setOrderId(getApplicationContext(),rideData.getOrderId());

                            if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {
                                 sendLocationBroadcast();
                                sendNotification(getApplicationContext(),notificationData.getBody(),notificationData.getTitle());


                            } else {
                                    sendNotificationOnly(getApplicationContext(),notificationData.getBody(),notificationData.getTitle());
      }
              }
                        else{
                            sendNotification(getApplicationContext(), notificationData.getBody(), notificationData.getTitle());
                        }

                    }
                });

                /* if (*//* Check if data needs to be processed by long running job *//* true) {
                    // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                    scheduleJob();
                } else {
                     handleNow();
                }*/
            }

        }}

    private void sendNotificationwithRide(String messageBody, String title, RideData rideData) {

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(NotificationManager.IMPORTANCE_HIGH);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }


    private void sendLocationBroadcast() {

        Intent intent = new Intent("IsquenyDriverApp");
        Bundle bundle = new Bundle();
        bundle.putBoolean("driverAssign", true);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent.putExtras(bundle));

    }


    private void scheduleJob() {

    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }
   private void sendNotification(Context context,String messageBody, String title) {
     Intent intent = new Intent(context, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationManager.IMPORTANCE_HIGH);
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }
    private void sendNotificationOnly(Context context,String messageBody, String title) {



        Intent intent = new Intent(context, HomeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("driverAssignOnly", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent.putExtras(bundle), PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationManager.IMPORTANCE_HIGH);
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

}