package comm.isqueni.vendor.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.activity.LoginActivity;

public class CommonHelper {
    KProgressHUD progressHUD;

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public static void ErrorDialog(Context context, String message) {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(context);
        //New Dialog Builder
        dialogBuilder
                .withTitle("Vendor App")
                .withTitleColor("#FFFFFF")
                .withMessage(message)
                .withMessageColor("#FFFFFF")
                .withDialogColor("#1FA7C4")
                .withDuration(600)
                .withEffect(Effectstype.Shake)
                .withButton1Text("OK")
                .isCancelableOnTouchOutside(true)
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogBuilder.dismiss();
                    }
                })
                .show();
    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    Log.i("Class", info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void showProgress(Context context, String string) {
        progressHUD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setLabel(string)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public void hideProgress() {
        if (progressHUD != null && progressHUD.isShowing()) {
            progressHUD.dismiss();
        }
    }

    public static RequestBody getRequestBodyParam(String value) {
        return RequestBody.create(MediaType.parse("text/form-data"), value);
    }
    public void errorhandling(Throwable e, Context context) {
        if (e instanceof HttpException) {
            HttpException error = (HttpException) e;
            int errorCode = error.response().code();
            if (errorCode == 401) {
                try {
                    String errorBody = error.response().errorBody().string();
                    JSONObject msgobj = new JSONObject(errorBody);
                    String msg = msgobj.optString("message");
                    Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                    SharedPreferenceManager.setisUserLoggin(context, false);
                    Intent intent = new Intent(context, LoginActivity.class);
                    ((Activity) context).startActivity(intent);
                    ((Activity) context).finish();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            } else if (errorCode == 406) {
                try {
                    String errorBody = error.response().errorBody().string();
                    JSONObject msgobj = new JSONObject(errorBody);
                    String msg = msgobj.optString("message");
                    Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            } else if (errorCode == 403) {
                try {
                    String errorBody = error.response().errorBody().string();
                    JSONObject msgobj = new JSONObject(errorBody);
                    String msg = msgobj.optString("message");
                    Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            } else if (errorCode == 500) {
                try {
                    String errorBody = error.response().errorBody().string();
                    JSONObject msgobj = new JSONObject(errorBody);
                    String msg = msgobj.optString("message");
                    Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            } else if (errorCode == 400) {
                try {
                    String errorBody = error.response().errorBody().string();
                    JSONObject msgobj = new JSONObject(errorBody);
                    String msg = msgobj.optString("message");
                    Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            } else if (errorCode == 405) {
                try {
                    String errorBody = error.response().errorBody().string();
                    JSONObject msgobj = new JSONObject(errorBody);
                    String msg = msgobj.optString("message");
                    Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }else if (errorCode == 404){
                try {
                    String errorBody = error.response().errorBody().string();
                    JSONObject msgobj = new JSONObject(errorBody);
                    String msg = msgobj.optString("message");
                    Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        } else {
            Toast.makeText(context, R.string.server_error, Toast.LENGTH_LONG).show();
        }
    }

    public static Bitmap createURiMarker(final Context context, String string) throws IOException {
        Bitmap bmp = null;
        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        ImageView markerImage = (ImageView) marker.findViewById(R.id.user_dp);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);

        return bitmap;
    }
    public static String getConvertedDate(String sDate){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",Locale.getDefault());
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());
        Date date = null;
        try {
            date = inputFormat.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = outputFormat.format(date);
        return formattedDate;
    }

    public static void ChangeButtonBb(Button button, Context context) {
        button.setBackground(context.getResources().getDrawable(R.drawable.white_et_bg));
        button.setTextColor(context.getResources().getColor(R.color.chec_box));
    }

    public static void RemooveFoucusBtn(Button button, Context context) {
        button.setBackground(context.getResources().getDrawable(R.drawable.white_bg_no_round));
        button.setTextColor(context.getResources().getColor(R.color.white));
    }
}
