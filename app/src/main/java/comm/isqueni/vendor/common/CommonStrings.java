package comm.isqueni.vendor.common;

public interface CommonStrings {

    public static final String SECRET_KEY = "mangifera18";
    public static final String DEVICE_TYPE = "android";
    public static final String DELETE_DRIVER = "delete_driver";
    public static final String ACTION_TAKEN = "action_taken";
    public static final String DO_ACTION = "do_action";
}
