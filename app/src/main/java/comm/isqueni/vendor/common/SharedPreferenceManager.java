package comm.isqueni.vendor.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by netset on 4/30/2018.
 */
@SuppressLint("ApplySharedPref")
public class SharedPreferenceManager {

    public static void setLanguageCheck(Context context, String language_check) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("language_check", language_check).commit();
    }

    public static String getLanguageCheck(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("language_check", "");

    }
    public static void setProfilePic(Context context, String profile_pic) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("profile_pic", profile_pic).commit();
    }


    public static String getProfilePic(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("profile_pic", "");

    }
    public static void setAuth(Context context, String setAuth) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("setAuth", setAuth).commit();
    }


    public static String getAuth(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("setAuth", "");

    }

    public static void setName(Context context, String name) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("name", name).commit();
    }

    public static String getName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("name", "");
    }
    public static void setisUserLoggin(Context context, boolean bool) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("bool", bool).commit();
    }

    public static boolean getisUserLoggin(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("bool", false);
    }

    public static void setVendorId(Context context, String vendor_id) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("vendor_id", vendor_id).commit();
    }

    public static String getVendorId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("vendor_id", "");
    }
    public static void setShopStatus(Context context, int shop_status) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("shop_status", shop_status).commit();
    }

    public static int getShopStatus(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("shop_status", 0);
    }
    public static void setOrderId(Context context, String order_id) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("order_id", order_id).commit();
    }

    public static String getOrderId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("order_id", "");
    }


    public static void setdeviceToken(Context context, String device_Token) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("device_Token", device_Token).commit();
    }

    public static String setdeviceToken(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("device_Token", "");
    }
}
