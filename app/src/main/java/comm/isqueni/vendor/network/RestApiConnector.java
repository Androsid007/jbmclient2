package comm.isqueni.vendor.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by netset on 4/30/2018.
 */

public class RestApiConnector {
    //Live URL
    public static String BASE_URL = "http://13.251.6.181:4009/rest_api/vendors/";
    private static Retrofit retrofit;
    private static HttpLoggingInterceptor mHttpLoggingInterceptor;


    private RestApiConnector() {

    }

    public static Retrofit getRetroInstance() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getClient())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getInstance() {

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;

    }


    private static OkHttpClient getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        Log.i("Interceptor", interceptor.toString());
        OkHttpClient okHttpClient;
        CookieHandler cookieHandler = new CookieManager();

        okHttpClient = new OkHttpClient.Builder().addNetworkInterceptor(interceptor)
                .cookieJar(new JavaNetCookieJar(cookieHandler))
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(getHttpLoggingInterceptor())
                .build();

        return okHttpClient;

    }

    private static HttpLoggingInterceptor getHttpLoggingInterceptor() {
        if (mHttpLoggingInterceptor == null) {
            mHttpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                @Override
                public void log(String message) {
                    System.out.println("OkHttp " + message);
                }
            });
            mHttpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
        return mHttpLoggingInterceptor;
    }
}
