package comm.isqueni.vendor.network;


import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import comm.isqueni.vendor.fragments.HomeFragment;
import comm.isqueni.vendor.fragments.MapFragment;
import comm.isqueni.vendor.fragments.OrderManagmentFragment;

/**
 * Created by netset on 4/30/2018.
 */

public class ApiRequestService {

    private Context context;
    private Retrofit retrofit;
    private RestApis restApis;


    public ApiRequestService(Context context) {
        this.context = context;
        retrofit = RestApiConnector.getRetroInstance();
        restApis = retrofit.create(RestApis.class);
    }

    public void callLoginApi(String header, String email,String password,String device_id,String lang,String device_type) {
        restApis.getLoginService(header,email,password,device_id,lang,device_type)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }
    public void callLogout(String header,String auth, String type) {
        restApis.getLogoutResponse(header,auth,type)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }
    public void callOpenProduct(String url,String header,String auth, String actiontype,String lang) {
        restApis.getOpenProduct(url,header,auth,actiontype,lang)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }
    public void callDriverManagement(String header,String auth, String actiontype,String lang) {
        restApis.getDriverManagement(header,auth,actiontype,lang)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }
    public void getDeleteDriver(String header,String auth, String actiontype) {
        restApis.getDeleteDriver(header,auth,actiontype)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }
    public void callProductManagement(String header, String auth, String actiontype, String lang) {
        restApis.getProductManagement(header,auth,actiontype,lang)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }
    public void callOrderManagment(String header, String auth, String actiontype, String lang, Fragment orderManagmentFragment) {
        restApis.getOrderManagment(header,auth,actiontype,lang)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) orderManagmentFragment);
    }
    public void callRecentOrder(String header, String auth, String actiontype, String lang, Fragment orderManagmentFragment) {
        restApis.getRecentOrder(header,auth,actiontype,lang)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) orderManagmentFragment);
    }
    public void callDriverList(String header, String auth, String actiontype, String lang, MapFragment mapFragment) {
        restApis.getDriverList(header,auth,actiontype,lang)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) mapFragment);
    }
    public void callAssignDriver(String header, String auth, String order_id, String lang,String driver_id, MapFragment mapFragment) {
        restApis.getAssignDriver(header,auth,order_id,lang,driver_id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) mapFragment);
    }

    public void callEmergency(String header,String auth,String language){
        restApis.getEmergency(header,auth,language)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }
    public void callActionTaken(String header,String auth,String driver_id,String orderid,String language){
        restApis.getActionNeeded(header,auth,driver_id,orderid,language)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }
    public void callSearch(String header,String auth,String search,String language){
        restApis.getSearch(header,auth,search,language)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }

    public void callForgot(String header,String auth,String email){
        restApis.getForgot(header,auth,email)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }

    public void callCancelOrder(String auth, String secretKey, String order_id, String lang, String reason) {
        restApis.getCancelOrder(auth,secretKey,order_id,lang,reason)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }

    public void callHomeData(String auth, String secretKey, String vendor_summary, String lang, HomeFragment homeFragment) {
        restApis.getHomeData(auth,secretKey,vendor_summary,lang)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) homeFragment);
    }

    public void callOrderHistory(String secretKey, String auth, String order_history, String lang) {
        restApis.getOrderHistory(secretKey,auth,order_history,lang)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }

    public void callProductDetail(String secretKey, String auth, String orderId, String lang) {
        restApis.getProductDetail(secretKey,auth,orderId,lang)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Observer) context);
    }
}
