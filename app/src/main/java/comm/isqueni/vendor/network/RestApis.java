package comm.isqueni.vendor.network;


import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Url;
import comm.isqueni.vendor.activity.ForgotPassword;
import comm.isqueni.vendor.models.DriverManagementResponse;
import comm.isqueni.vendor.models.EmergencyRequestResponse;
import comm.isqueni.vendor.models.HomeResponse;
import comm.isqueni.vendor.models.LoginResponse;
import comm.isqueni.vendor.models.LogoutResponse;
import comm.isqueni.vendor.models.MapDriverResponse;
import comm.isqueni.vendor.models.OrderHistoryResponse;
import comm.isqueni.vendor.models.OrderManagmentResponse;
import comm.isqueni.vendor.models.ProductDetailResponse;
import comm.isqueni.vendor.models.ProductListResponse;
import comm.isqueni.vendor.models.RecentOrderResponse;

/**
 * Created by netset on 4/30/2018.
 */

public interface RestApis {
    // @Header("appVersion") String appVersion

    @FormUrlEncoded
    @POST("login")
    Observable<LoginResponse> getLoginService(@Header("secret") String secretKey,
                                              @Field("email") String email,
                                              @Field("password") String password,
                                              @Field("device_token") String device_token,
                                              @Field("language") String language,
                                              @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("logout")
    Observable<LogoutResponse> getLogoutResponse(@Header("secret") String secretKey,
                                                 @Header("authorization") String auth,
                                                 @Field("logout_type") String email);

    @FormUrlEncoded
    @POST
    Observable<LogoutResponse> getOpenProduct(@Url String url,
                                              @Header("secret") String secretKey,
                                              @Header("authorization") String auth,
                                              @Field("action_type") String action_type,
                                              @Field("language") String language);

    @FormUrlEncoded
    @POST("driver_all_list")
    Observable<DriverManagementResponse> getDriverManagement(@Header("secret") String secretKey,
                                                             @Header("authorization") String auth,
                                                             @Field("action_type") String action_type,
                                                             @Field("language") String language);

    @FormUrlEncoded
    @POST("driver_delete")
    Observable<LogoutResponse> getDeleteDriver(@Header("secret") String secretKey,
                                               @Header("authorization") String auth,
                                               @Field("driver_id") String driver_id);

    @FormUrlEncoded
    @POST("product_list")
    Observable<ProductListResponse> getProductManagement(@Header("secret") String secretKey,
                                                         @Header("authorization") String auth,
                                                         @Field("action_type") String action_type,
                                                         @Field("language") String language);

    @FormUrlEncoded
    @POST("order_list")
    Observable<OrderManagmentResponse> getOrderManagment(@Header("secret") String secretKey,
                                                         @Header("authorization") String auth,
                                                         @Field("action_type") String action_type,
                                                         @Field("language") String language);

    @FormUrlEncoded
    @POST("order_placed")
    Observable<RecentOrderResponse> getRecentOrder(@Header("secret") String secretKey,
                                                   @Header("authorization") String auth,
                                                   @Field("action_type") String action_type,
                                                   @Field("language") String language);

    @FormUrlEncoded
    @POST("order_history")
    Observable<OrderHistoryResponse> getOrderHistory(@Header("secret") String secretKey,
                                                     @Header("authorization") String auth,
                                                     @Field("action_type") String action_type,
                                                     @Field("language") String language);

    @FormUrlEncoded
    @POST("driver_active_list")
    Observable<MapDriverResponse> getDriverList(@Header("secret") String secretKey,
                                                @Header("authorization") String auth,
                                                @Field("action_type") String action_type,
                                                @Field("language") String language);

    @FormUrlEncoded
    @POST("driver_assign_order")
    Observable<LogoutResponse> getAssignDriver(@Header("secret") String secretKey,
                                               @Header("authorization") String auth,
                                               @Field("order_id") String action_type,
                                               @Field("language") String language,
                                               @Field("driver_id") String driver_id);


    @FormUrlEncoded
    @POST("emergency_request")
    Observable<EmergencyRequestResponse> getEmergency(@Header("secret") String secretKey,
                                                      @Header("authorization") String auth,
                                                      @Field("language") String language);

    @FormUrlEncoded
    @POST("emergency_action_needed")
    Observable<LogoutResponse> getActionNeeded(@Header("secret") String secretKey,
                                               @Header("authorization") String auth,
                                               @Field("driver_id") String driver_id,
                                               @Field("order_id") String order_id,
                                               @Field("language") String language);

    @FormUrlEncoded
    @POST("product_show_by_name")
    Observable<ProductListResponse> getSearch(@Header("secret") String secretKey,
                                              @Header("authorization") String auth,
                                              @Field("product_name") String driver_id,
                                              @Field("language") String language);

    @FormUrlEncoded
    @POST("forgot_password")
    Observable<LogoutResponse> getForgot(@Header("secret") String secretKey,
                                         @Header("authorization") String auth,
                                         @Field("email") String driver_id);

    @FormUrlEncoded
    @POST("order_cancel_with_reason")
    Observable<LogoutResponse> getCancelOrder(@Header("authorization") String auth,
                                              @Header("secret") String secretKey,
                                              @Field("order_id") String order_id,
                                              @Field("language") String language,
                                              @Field("reason") String reason);

    @FormUrlEncoded
    @POST("vendor_summary")
    Observable<HomeResponse> getHomeData(@Header("authorization") String auth,
                                         @Header("secret") String secretKey,
                                         @Field("action_type") String order_id,
                                         @Field("language") String languag);

    @FormUrlEncoded
    @POST("product_details")
    Observable<ProductDetailResponse> getProductDetail( @Header("secret") String secretKey,
                                                        @Header("authorization") String auth,
                                                       @Field("product_id") String order_id,
                                                       @Field("language") String languag);
}
