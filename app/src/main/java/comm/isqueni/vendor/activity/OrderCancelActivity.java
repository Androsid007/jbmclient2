package comm.isqueni.vendor.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.adapter.CancelOrderAdapter;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.interf.onRecyclerViewClick;
import comm.isqueni.vendor.models.CancelOrderList;
import comm.isqueni.vendor.models.LogoutResponse;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class OrderCancelActivity extends AppCompatActivity implements onRecyclerViewClick, Observer {

    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    Button btn_next;
    EditText et_info;
    RecyclerView recyclerView;
    ImageView iv_back;
    CancelOrderAdapter cancelOrderAdapter;
    List<CancelOrderList> cancelOrderListList = new ArrayList<>();
    private int reason = 0;
    private String order_id;
    private String lang;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancel_order_bg);
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

            order_id = SharedPreferenceManager.getOrderId(this);

        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(this);

        init();
        onClick();
    }

    private void init() {
        btn_next = findViewById(R.id.submitBtn);
        iv_back = findViewById(R.id.back_arrow);
        et_info = findViewById(R.id.et_info);
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        CancelOrderList cancelOrderList = new CancelOrderList("Product not available at the moment.");
        cancelOrderListList.add(cancelOrderList);
        cancelOrderList = new CancelOrderList("There is no driver available");
        cancelOrderListList.add(cancelOrderList);
        cancelOrderList = new CancelOrderList("The request quantity is not available.");
        cancelOrderListList.add(cancelOrderList);
        cancelOrderList = new CancelOrderList("Rejected by client.");
        cancelOrderListList.add(cancelOrderList);

        cancelOrderAdapter = new CancelOrderAdapter(cancelOrderListList, this, OrderCancelActivity.this);
        recyclerView.setAdapter(cancelOrderAdapter);

    }

    private void onClick() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reason > 0) {
                    callCancelApi();
                } else {
                    Toast.makeText(getApplicationContext(), "Please select a reason.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void callCancelApi() {
        if (SharedPreferenceManager.getLanguageCheck(this).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }
        commonHelper.showProgress(this,getString(R.string.show_prog));
        apiRequestService.callCancelOrder(SharedPreferenceManager.getAuth(this),SECRET_KEY,order_id,lang,""+reason);
    }

    @Override
    public void onRecyClick(int pos) {
        if (pos == 3) {
            reason = 4;
            et_info.setVisibility(View.VISIBLE);
        } else {
            reason = pos + 1;
            et_info.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof LogoutResponse){
            LogoutResponse  logoutResponse = (LogoutResponse)value;
            if (logoutResponse.getStatus()==1){
                Toast.makeText(this,logoutResponse.getMessage(),Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        }

    }

    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e,this);
    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }
}
