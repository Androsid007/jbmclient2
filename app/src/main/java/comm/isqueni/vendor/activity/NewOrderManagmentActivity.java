package comm.isqueni.vendor.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import comm.isqueni.vendor.R;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.fragments.HomeFragment;
import comm.isqueni.vendor.fragments.OrderManagmentFragment;
import comm.isqueni.vendor.fragments.RecentOrderFragment;

public class NewOrderManagmentActivity extends AppCompatActivity {

    ImageView iv_back;
    Button btn_recent_order,btn_order;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_managment_new);
        if (savedInstanceState == null) {
            Fragment fragment = new OrderManagmentFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flContent, fragment);
            ft.commit();
        }
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        init();
        conClick();
    }

    private void conClick() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_recent_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonHelper.ChangeButtonBb(btn_recent_order,getApplicationContext());
                CommonHelper.RemooveFoucusBtn(btn_order,getApplicationContext());
                Fragment fragment = new RecentOrderFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flContent, fragment);
                ft.commit();
            }
        });
        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonHelper.ChangeButtonBb(btn_order,getApplicationContext());
                CommonHelper.RemooveFoucusBtn(btn_recent_order,getApplicationContext());
                Fragment fragment = new OrderManagmentFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.flContent, fragment);
                ft.commit();
            }
        });
    }

    private void init() {
        iv_back = findViewById(R.id.back_arrow);
        btn_order = findViewById(R.id.order_btn);
        btn_recent_order = findViewById(R.id.recentOrder_btn);
    }
}
