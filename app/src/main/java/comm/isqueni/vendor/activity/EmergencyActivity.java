package comm.isqueni.vendor.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.adapter.EmergencyAdapter;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.models.EmergencyRequestResponse;
import comm.isqueni.vendor.models.EmergencyRequestSearch;
import comm.isqueni.vendor.models.LogoutResponse;
import comm.isqueni.vendor.models.OrderList;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.DO_ACTION;
import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class EmergencyActivity extends AppCompatActivity implements Observer, EmergencyAdapter.onEmergencyClick {

    private ImageView iv_back;
    RecyclerView emergencyRv;
    EmergencyAdapter emergencyAdapter;
    List<EmergencyRequestSearch> requestSearches = new ArrayList<>();
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    private String lang;
    EditText et_serach;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(this);
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        initViews();
        onClick();
        if (SharedPreferenceManager.getLanguageCheck(this).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }
        callEmergency();
    }

    private void callEmergency() {
        commonHelper.showProgress(this, getString(R.string.show_prog));
        apiRequestService.callEmergency(SECRET_KEY, SharedPreferenceManager.getAuth(this), lang);
    }

    private void initViews() {
        iv_back = findViewById(R.id.back_arrow);
        et_serach = findViewById(R.id.et_serach);
        emergencyRv = findViewById(R.id.emergencyRv);
        emergencyRv.setHasFixedSize(false);
        emergencyRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void onClick() {
        iv_back.setOnClickListener(v -> onBackPressed());
        et_serach.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    private void filter(String s) {
        List<EmergencyRequestSearch> filteredList = new ArrayList<>();
        for (EmergencyRequestSearch row : requestSearches) {
            if (row.getDriverName() != null)
                if (row.getDriverName().toLowerCase().contains(s.toLowerCase())) {
                    filteredList.add(row);
                }
        }
        emergencyAdapter.getFilter(filteredList);
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof EmergencyRequestResponse) {
            EmergencyRequestResponse response = (EmergencyRequestResponse) value;
            if (response.getStatus() == 1) {
                requestSearches = response.getEmergencyRequestSearch();
                if (requestSearches != null && requestSearches.size() > 0) {
                    emergencyAdapter = new EmergencyAdapter(this, requestSearches, EmergencyActivity.this);
                    emergencyRv.setAdapter(emergencyAdapter);
                }
            }
        } else if (value instanceof LogoutResponse) {
            LogoutResponse logoutResponse = (LogoutResponse) value;
            if (logoutResponse.getStatus() == 1) {
                Toast.makeText(this, logoutResponse.getMessage(), Toast.LENGTH_LONG).show();
                callEmergency();
            }
        }

    }

    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e, this);
    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }

    @Override
    public void onEmergency(int pos, String s) {
        if (!s.equals(DO_ACTION)) {
            commonHelper.showProgress(this, getString(R.string.show_prog));
            apiRequestService.callActionTaken(SECRET_KEY, SharedPreferenceManager.getAuth(this), "" + requestSearches.get(pos).getDriverId(),
                    requestSearches.get(pos).getOrder_id(), lang);
        } else {

        }

    }
}
