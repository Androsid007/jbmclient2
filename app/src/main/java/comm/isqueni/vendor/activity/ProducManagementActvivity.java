package comm.isqueni.vendor.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.adapter.ProductManagmentAdapter;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.interf.onRecyclerViewClick;
import comm.isqueni.vendor.models.ProductListResponse;
import comm.isqueni.vendor.models.ProductSearch;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class ProducManagementActvivity extends AppCompatActivity implements Observer, onRecyclerViewClick {

    RecyclerView product_rv;
    EditText et_search;
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    private String lang;
    ImageView iv_back, searchIv;
    ProductManagmentAdapter managmentAdapter;
    List<ProductSearch> productSearches = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
        }
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(this);
        init();
        onClick();
        if (SharedPreferenceManager.getLanguageCheck(this).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }
        callProductList();
    }

    private void callProductList() {
        commonHelper.showProgress(this, getString(R.string.show_prog));
        apiRequestService.callProductManagement(SECRET_KEY, SharedPreferenceManager.getAuth(this), "product_list", lang);
    }

    private void onClick() {
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // managmentAdapter.getFilter().filter(s.toString());
                filter(s.toString());
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void filter(String text) {
        List<ProductSearch> filteredList = new ArrayList<>();
        for (ProductSearch row : productSearches) {
            if (row.getName() != null)
                if (row.getName().toLowerCase().contains(text.toLowerCase())) {
                    filteredList.add(row);
                }
        }
        managmentAdapter.filterList(filteredList);
    }

    private void callSearchapi(String trim) {
        commonHelper.showProgress(this, getString(R.string.show_prog));
        apiRequestService.callSearch(SECRET_KEY, SharedPreferenceManager.getAuth(this),
                trim, lang);
    }

    private void init() {
        //EditText
        et_search = findViewById(R.id.et_search);

        iv_back = findViewById(R.id.back_arrow);
        //  searchIv  =findViewById(R.id.searchIv);
        //Recyclerview
        product_rv = findViewById(R.id.product_rv);
        product_rv.setHasFixedSize(true);
        product_rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof ProductListResponse) {
            ProductListResponse listResponse = (ProductListResponse) value;
            if (listResponse.getStatus() == 1) {
                Toast.makeText(this, listResponse.getMessage(), Toast.LENGTH_LONG).show();
                productSearches = listResponse.getProductSearch();
                if (listResponse.getProductSearch() != null && listResponse.getProductSearch().size() > 0) {
                    managmentAdapter = new ProductManagmentAdapter(listResponse.getProductSearch(), this, ProducManagementActvivity.this);
                    product_rv.setAdapter(managmentAdapter);
                }
            }
        }

    }

    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e, this);
    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }

    @Override
    public void onRecyClick(int pos) {
        SharedPreferenceManager.setOrderId(this, "" + productSearches.get(pos).getProductId());
        startActivity(new Intent(this, ProductDetailActivity.class));
    }
}
