package comm.isqueni.vendor.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.adapter.DriverManagmentAdapter;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.models.DriverManagementResponse;
import comm.isqueni.vendor.models.DriverSearch;
import comm.isqueni.vendor.models.LogoutResponse;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class DriverManagementActivity extends AppCompatActivity implements Observer, DriverManagmentAdapter.onRecyclerviewDelete {

    RecyclerView driverManagementRv;
    private ImageView add_product;
    ImageView iv_back;
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    private String lang;
    EditText et_search;
    List<DriverSearch> driverSearches = new ArrayList<>();
    DriverManagmentAdapter driverManagmentAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_management);
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
        }
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(this);
        init();
        onClick();
        if (SharedPreferenceManager.getLanguageCheck(this).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }
        callDriverDetail();
    }

    private void callDriverDetail() {
        commonHelper.showProgress(this, getString(R.string.show_prog));
        apiRequestService.callDriverManagement(SECRET_KEY, SharedPreferenceManager.getAuth(this), "driver_all_list", lang);
    }

    private void onClick() {

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    private void filter(String text) {
        List<DriverSearch> filteredList = new ArrayList<>();
        for (DriverSearch row : driverSearches) {
            if (row.getName() != null && row.getDriverLicenceNumber()!=null && row.getMobile()!=null)
                if (row.getName().toLowerCase().contains(text.toLowerCase()) || row.getDriverLicenceNumber().toLowerCase().contains(text.toLowerCase()) || row.getMobile().toLowerCase().contains(text.toLowerCase())) {
                    filteredList.add(row);
                }
        }
        driverManagmentAdapter.getFilter(filteredList);
    }

    private void init() {
        //Recyclerview
        driverManagementRv = findViewById(R.id.driverManagementRv);
        et_search = findViewById(R.id.et_search);
        driverManagementRv.setHasFixedSize(true);
        driverManagementRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        //Image View
        iv_back = findViewById(R.id.back_arrow);

    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof DriverManagementResponse) {
            DriverManagementResponse managementResponse = (DriverManagementResponse) value;
            if (managementResponse.getStatus() == 1) {
                if (managementResponse.getDriverSearch() != null && managementResponse.getDriverSearch().size() > 0) {
                    driverSearches = managementResponse.getDriverSearch();
                    driverManagmentAdapter = new DriverManagmentAdapter(managementResponse.getDriverSearch(), this, DriverManagementActivity.this);
                    driverManagementRv.setAdapter(driverManagmentAdapter);
                    driverManagmentAdapter.notifyDataSetChanged();
                }
            }
        } else if (value instanceof LogoutResponse) {
            LogoutResponse logoutResponse = (LogoutResponse) value;
            if (logoutResponse.getStatus() == 1) {
                callDriverDetail();
            }
        }

    }

    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e, this);

    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }

    @Override
    public void onClickDelete(int pos, String key) {
        commonHelper.showProgress(this, getString(R.string.show_prog));
        apiRequestService.getDeleteDriver(SECRET_KEY, SharedPreferenceManager.getAuth(this), String.valueOf(driverSearches.get(pos).getDriverId()));
    }
}
