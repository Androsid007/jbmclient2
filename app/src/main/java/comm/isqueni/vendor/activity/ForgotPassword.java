package comm.isqueni.vendor.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.models.LogoutResponse;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class ForgotPassword extends AppCompatActivity implements Observer {

    EditText et_email;
    Button btn_frgt;

    ImageView iv_back;
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pwd_activity);

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        init();
        onClick();
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(this);
    }

    private void onClick() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_frgt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                   callForogotPwd();
                }
            }
        });
    }

    private void callForogotPwd() {
        commonHelper.showProgress(this,getString(R.string.show_prog));
        apiRequestService.callForgot(SECRET_KEY, SharedPreferenceManager.getAuth(this),et_email.getText().toString().trim());
    }

    private boolean validate() {
        String email = et_email.getText().toString();

        if (email.isEmpty()) {
            CommonHelper.ErrorDialog(this, getString(R.string.email_error));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            CommonHelper.ErrorDialog(this, getString(R.string.email_vlaid_error));
            return false;
        }
        return true;
    }

    private void init() {
        iv_back = findViewById(R.id.back_arrow);

        et_email = findViewById(R.id.et_email);
        btn_frgt = findViewById(R.id.btn_reset);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof LogoutResponse){
            LogoutResponse logoutResponse = (LogoutResponse)value;
            if (logoutResponse.getStatus()==1){
                Toast.makeText(this,logoutResponse.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e,this);
    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }
}
