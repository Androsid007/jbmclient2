package comm.isqueni.vendor.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.models.ProductDetailResponse;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class ProductDetailActivity extends AppCompatActivity implements Observer {

    TextView description_tv, calories_tv, color_tv, price_tv, size_tv, product_tv;
    ImageView productImage, iv_back;
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    String lang;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail_activity);


        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        if (SharedPreferenceManager.getLanguageCheck(this).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(this);

        callProductDetail();
        init();
        onClick();
    }

    private void onClick() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void init() {
        description_tv = findViewById(R.id.description_tv);
        calories_tv = findViewById(R.id.calories_tv);
        color_tv = findViewById(R.id.color_tv);
        price_tv = findViewById(R.id.price_tv);
        size_tv = findViewById(R.id.size_tv);
        product_tv = findViewById(R.id.product_tv);
        productImage = findViewById(R.id.productImage);
        iv_back = findViewById(R.id.back_arrow);
    }

 /*   @Override
    protected void onResume() {
        super.onResume();
        callProductDetail();
    }
*/
    private void callProductDetail() {
        commonHelper.showProgress(this, getString(R.string.show_prog));
        apiRequestService.callProductDetail(SECRET_KEY,SharedPreferenceManager.getAuth(this), SharedPreferenceManager.getOrderId(this),lang);
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof ProductDetailResponse) {
            ProductDetailResponse detailResponse = (ProductDetailResponse) value;
            if (detailResponse.getStatus() == 1) {
                if (detailResponse.getProductDetails() != null) {
                    if (detailResponse.getProductDetails().getPhoto() != null) {
                        Picasso.with(this).load(detailResponse.getProductDetails().getPhoto())
                                .placeholder(R.drawable.ic_no_image)
                                .resize(1080, 720)
                                .centerCrop()
                                .into(productImage);
                    }
                    if (detailResponse.getProductDetails().getName() != null) {
                        product_tv.setText(detailResponse.getProductDetails().getName());
                    }
                    if (detailResponse.getProductDetails().getPriceCorp() != null) {
                        price_tv.setText("" + detailResponse.getProductDetails().getPriceCorp());
                    }
                    if (detailResponse.getProductDetails().getChemicalDetails() != null && detailResponse.getProductDetails().getChemicalDetails().size() > 0) {
                        String chemical = "";
                        for (int i = 0; i < detailResponse.getProductDetails().getChemicalDetails().size(); i++) {
                            if (chemical.equals("")) {
                                chemical = detailResponse.getProductDetails().getChemicalDetails().get(i).getChemicalName() + " : " + detailResponse.getProductDetails().getChemicalDetails().get(i).getChemicalValue();
                            } else {
                                chemical = chemical + "\n" + detailResponse.getProductDetails().getChemicalDetails().get(i).getChemicalName() + " : " + detailResponse.getProductDetails().getChemicalDetails().get(i).getChemicalValue();
                            }

                        }
                        calories_tv.setText(chemical);
                    }
                    if (detailResponse.getProductDetails().getText()!=null){
                        description_tv.setText(detailResponse.getProductDetails().getText());
                    }
                }
            }
        }
    }

    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e,this);
    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }
}
