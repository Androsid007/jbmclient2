package comm.isqueni.vendor.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.adapter.OrderHistoryAdapter;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.models.OrderHistory;
import comm.isqueni.vendor.models.OrderHistoryResponse;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class OrderHistoryActivty extends AppCompatActivity implements Observer {

    List<OrderHistory> orderHistories = new ArrayList<>();
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    ImageView iv_back;
    RecyclerView order_rv;
    private String lang;
    EditText et_serach;
    OrderHistoryAdapter orderHistoryAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_history_activity);
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(this);

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        if (SharedPreferenceManager.getLanguageCheck(this).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }

        init();
        onClick();
    }

    @Override
    protected void onResume() {
        super.onResume();
        callOrderHistory();
    }

    private void callOrderHistory() {
        commonHelper.showProgress(this, getString(R.string.show_prog));
        apiRequestService.callOrderHistory(SECRET_KEY, SharedPreferenceManager.getAuth(this), "order_history", lang);

    }

    private void onClick() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        et_serach.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    private void filter(String s) {
        List<OrderHistory> filteredList = new ArrayList<>();
        for (OrderHistory row : orderHistories) {
            if (row.getProductName() != null)
                if (row.getProductName().toLowerCase().contains(s.toLowerCase())) {
                    filteredList.add(row);
                }
        }
        orderHistoryAdapter.getFilter(filteredList);
    }

    private void init() {
        iv_back = findViewById(R.id.back_arrow);
        order_rv = findViewById(R.id.order_rv);
        et_serach = findViewById(R.id.et_serach);
        order_rv.setHasFixedSize(true);
        order_rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof OrderHistoryResponse) {
            OrderHistoryResponse historyResponse = (OrderHistoryResponse) value;
            if (historyResponse.getStatus() == 1) {
                orderHistories = historyResponse.getOrderHistory();
                if (orderHistories != null && orderHistories.size() > 0) {
                    orderHistoryAdapter = new OrderHistoryAdapter(orderHistories,this);
                    order_rv.setAdapter(orderHistoryAdapter);

                }
            }
        }

    }

    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e,this);
    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }


}
