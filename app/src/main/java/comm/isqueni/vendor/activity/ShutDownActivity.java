package comm.isqueni.vendor.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.suke.widget.SwitchButton;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.models.LogoutResponse;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class ShutDownActivity extends AppCompatActivity implements Observer {
    ImageView iv_back;
    SwitchButton switch_button;
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    private String lang;
    private String url;
    private String actyion_type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shut_down_activity);
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(this);
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        if (SharedPreferenceManager.getLanguageCheck(this).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }
        init();
        onClick();
        if (SharedPreferenceManager.getShopStatus(this)==1){
            switch_button.setChecked(true);
        }else {
            switch_button.setChecked(false);
        }

    }

    private void onClick() {
        switch_button.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (isChecked) {
                    SharedPreferenceManager.setShopStatus(getApplicationContext(),1);

                    url = "vendor_showdown_open";
                    actyion_type = "vendor_shodwon_open";
                    callshudwonservice();
                } else {
                    SharedPreferenceManager.setShopStatus(getApplicationContext(),0);
                    url = "vendor_showdown_close";
                    actyion_type = "vendor_shodwon";
                    callshudwonservice();

                }
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void callshudwonservice() {
        commonHelper.showProgress(this, getString(R.string.show_prog));
        apiRequestService.callOpenProduct(url, SECRET_KEY, SharedPreferenceManager.getAuth(this), actyion_type, lang);
    }


    private void init() {
        switch_button = findViewById(R.id.switch_button);
        iv_back = findViewById(R.id.back_arrow);
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof LogoutResponse) {
            LogoutResponse logoutResponse = (LogoutResponse) value;
            if (logoutResponse.getStatus()==1){
                Toast.makeText(this,logoutResponse.getMessage(),Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        }

    }

    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e,this);
    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }
}
