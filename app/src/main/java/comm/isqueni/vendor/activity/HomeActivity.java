package comm.isqueni.vendor.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.AlertView;
import com.irozon.alertview.objects.AlertAction;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.adapter.NavigationViewAdapter;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.fragments.HomeFragment;
import comm.isqueni.vendor.fragments.SettingFragment;
import comm.isqueni.vendor.interf.onRecyclerViewClick;
import comm.isqueni.vendor.models.LogoutResponse;
import comm.isqueni.vendor.models.NavigationViewList;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class HomeActivity extends AppCompatActivity implements onRecyclerViewClick, Observer {
    DrawerLayout drawer;
    ImageView nav_ham;
    RecyclerView recyclerView;
    List<NavigationViewList> navigationViewList = new ArrayList<>();
    ImageView profile_image;
    TextView name_tv;
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    private boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(this);
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
        }
        if (savedInstanceState == null) {
            Fragment fragment = new HomeFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flContent, fragment);
            ft.commit();
        }
        init();
        onClick();
    }

    private void onClick() {
        nav_ham.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        fill_profile_data();
    }

    private void fill_profile_data() {
        if (SharedPreferenceManager.getName(this) != null && !SharedPreferenceManager.getName(this).equals("")) {
            name_tv.setText(SharedPreferenceManager.getName(this));
        }
        if (SharedPreferenceManager.getProfilePic(this) != null && !SharedPreferenceManager.getProfilePic(this).equals("")) {
            /* name_tv.setText(SharedPreferenceManager.getName(this));*/
            Picasso.with(this).load(SharedPreferenceManager.getProfilePic(this)).centerCrop()
                    .resize(1080, 720).into(profile_image);
        }

    }

    private void init() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_ham = findViewById(R.id.nav_ham);
        recyclerView = findViewById(R.id.recyclerView);
        profile_image = findViewById(R.id.imageView);
        name_tv = findViewById(R.id.name_tv);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        NavigationViewAdapter navigationViewAdapter = new NavigationViewAdapter(navigationViewList, this, HomeActivity.this);
        recyclerView.setAdapter(navigationViewAdapter);

        NavigationViewList navView = new NavigationViewList(getString(R.string.home));
        navigationViewList.add(navView);
        navView = new NavigationViewList(getString(R.string.driver_management));
        navigationViewList.add(navView);
        navView = new NavigationViewList(getString(R.string.order_management));
        navigationViewList.add(navView);
        navView = new NavigationViewList(getString(R.string.product_management));
        navigationViewList.add(navView);
        navView = new NavigationViewList(getString(R.string.risk_management));
        navigationViewList.add(navView);
        navView = new NavigationViewList(getString(R.string.history_order));
        navigationViewList.add(navView);
        navView = new NavigationViewList(getString(R.string.setting));
        navigationViewList.add(navView);
        navView = new NavigationViewList((getString(R.string.logout)));
        navigationViewList.add(navView);
    }

   /* @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }*/


    @Override
    public void onRecyClick(int pos) {
        if (pos == 7) {
            drawer.closeDrawers();
            AlertView alert = new AlertView(getString(R.string.logout), getString(R.string.logout_check), AlertStyle.DIALOG);
            alert.addAction(new AlertAction(getString(R.string.yes), AlertActionStyle.DEFAULT, action -> {
                callLogoutService();

            }));
            alert.addAction(new AlertAction(getString(R.string.no), AlertActionStyle.DEFAULT, action -> {

            }));


            alert.show(this);
        } else if (pos == 1) {
            drawer.closeDrawers();
            startActivity(new Intent(this, DriverManagementActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (pos == 2) {
            drawer.closeDrawers();
            startActivity(new Intent(this, NewOrderManagmentActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (pos == 3) {
            drawer.closeDrawers();
            startActivity(new Intent(this, ProducManagementActvivity.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (pos == 4) {
            drawer.closeDrawers();
            startActivity(new Intent(this, EmergencyActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (pos == 6) {
            drawer.closeDrawers();
            Fragment fragment = new SettingFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction().addToBackStack(null);
            ft.replace(R.id.flContent, fragment);
            ft.commit();
        } else if (pos == 5) {
            drawer.closeDrawers();
            startActivity(new Intent(this, OrderHistoryActivty.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        else if (pos == 0) {
            drawer.closeDrawers();
            Fragment fragment = new HomeFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction().addToBackStack(null);
            ft.replace(R.id.flContent, fragment);
            ft.commit();
        }else {
            drawer.closeDrawers();
        }

    }

    private void callLogoutService() {
        commonHelper.showProgress(this, getString(R.string.show_prog));
        apiRequestService.callLogout(SECRET_KEY, SharedPreferenceManager.getAuth(this), "Vendor");
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof LogoutResponse) {
            LogoutResponse logoutResponse = (LogoutResponse) value;
            if (logoutResponse.getStatus() == 1) {
                SharedPreferenceManager.setisUserLoggin(this, false);
                SharedPreferenceManager.setName(this, "");
                SharedPreferenceManager.setProfilePic(this, "");
                SharedPreferenceManager.setAuth(this, "");
                Toast.makeText(this, logoutResponse.getMessage(), Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finishAffinity();
            }
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                finish();
            } else {
                if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, R.string.back_ecit, Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);

                }

            }

        }
    }


    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e, this);

    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }
}
