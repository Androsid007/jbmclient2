package comm.isqueni.vendor.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.RequestBody;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.models.LoginResponse;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.DEVICE_TYPE;
import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class LoginActivity extends AppCompatActivity implements Observer {

    EditText et_email, et_password;
    TextView tv_frgtpwd;
    Button btn_login;
    ImageView iv_back;
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    int lang = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(this);

        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            CommonHelper.setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        if (SharedPreferenceManager.getLanguageCheck(this).equals("ar")) {
            lang = 1;
        } else {
            lang = 2;
        }

        init();
        onClick();
    }

    private void onClick() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    if (CommonHelper.isNetworkAvailable(getApplicationContext())) {
                        callLoginService();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please Check Internet Connectivity.", Toast.LENGTH_LONG).show();
                    }
                    /* dostartActivity(HomeActivity.class);*/
                }
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_frgtpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dostartActivity(ForgotPassword.class);
            }
        });
    }

    private void callLoginService() {
        commonHelper.showProgress(this, getString(R.string.show_prog));
        apiRequestService.callLoginApi(SECRET_KEY,et_email.getText().toString().trim(),
                et_password.getText().toString().trim(),"nfkjdsfjd",String.valueOf(lang),DEVICE_TYPE);

    }

    private void dostartActivity(Class<?> activity) {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    private boolean validate() {
        String email = et_email.getText().toString().trim();
        String password = et_password.getText().toString().trim();

        if (email.isEmpty()) {
            CommonHelper.ErrorDialog(this, getString(R.string.email_error));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            CommonHelper.ErrorDialog(this, getString(R.string.email_vlaid_error));
            return false;
        } else if (password.isEmpty()) {
            CommonHelper.ErrorDialog(this, getString(R.string.password_error));
            return false;
        }
        return true;
    }

    private void init() {
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        tv_frgtpwd = findViewById(R.id.forgotPwdTv);
        btn_login = findViewById(R.id.btn_login);
        iv_back = findViewById(R.id.back_arrow);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        if (value instanceof LoginResponse) {
            commonHelper.hideProgress();
            LoginResponse loginResponse = (LoginResponse) value;
            if (loginResponse.getStatus()==1){
                SharedPreferenceManager.setVendorId(this, String.valueOf(loginResponse.getVendorId()));
                SharedPreferenceManager.setisUserLoggin(this,true);
                SharedPreferenceManager.setName(this,loginResponse.getName());
                SharedPreferenceManager.setAuth(this,loginResponse.getToken());
                SharedPreferenceManager.setProfilePic(this,loginResponse.getPhoto());
                dostartActivity(HomeActivity.class);
                finishAffinity();
            }else {
                SharedPreferenceManager.setisUserLoggin(this,false);
                Toast.makeText(this,loginResponse.getMessage(),Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e, this);
    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }
}
