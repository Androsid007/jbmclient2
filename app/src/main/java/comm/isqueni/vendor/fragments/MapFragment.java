package comm.isqueni.vendor.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.irozon.alertview.AlertActionStyle;
import com.irozon.alertview.AlertStyle;
import com.irozon.alertview.AlertView;
import com.irozon.alertview.objects.AlertAction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.GPSTracker;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.models.LogoutResponse;
import comm.isqueni.vendor.models.MapDriverList;
import comm.isqueni.vendor.models.MapDriverResponse;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener
        , Observer {

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    ArrayList<LatLng> markerPoints;
    private static SupportMapFragment fragment;
    private GPSTracker gps;
    View mapView;
    static LatLng latLng;

    FragmentManager fm;

    List<MapDriverList> driverSearch = new ArrayList<>();
    Marker listMarker;
    private HashMap<Marker, Integer> mHashMap = new HashMap<Marker, Integer>();
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    private String lang;

    public MapFragment() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map, container, false);
        fm = getChildFragmentManager();
        fragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (fragment == null) {
            fragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, fragment).commit();
            mapView = fragment.getView();
        }
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(getActivity());
        gps = new GPSTracker(getActivity());
        gps.startTraking();

        markerPoints = new ArrayList<LatLng>();
        if (SharedPreferenceManager.getLanguageCheck(getActivity()).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }


        return view;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (getActivity() != null)
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            location = mMap.getMyLocation();
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        try {
            latLng = new LatLng(location.getLatitude(), location.getLongitude());
            callDriverList();
        } catch (Exception e) {
            Log.e("Map_exception", String.valueOf(e));
        }
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {

        } catch (Resources.NotFoundException e) {
            Log.e("", "Can't find style. Error: ", e);
        }
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);

            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
        latLng = new LatLng(gps.getLatitude(), gps.getLongitude());


        try {


            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);

            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(null));

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
            mMap.animateCamera(cameraUpdate);


        } catch (
                Exception e)

        {
            Log.e("Map_exception", String.valueOf(e));
        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()

        {

            @Override
            public void onMapClick(LatLng point) {

            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()

        {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (mHashMap != null && mHashMap.size() > 0) {
                    show_map_driver_assign(driverSearch.get(mHashMap.get(marker)));

                }
                return false;
            }
        });

    }

    private void show_map_driver_assign(MapDriverList mapDriverList) {
        AlertView alert = new AlertView(getString(R.string.assign_driver), getString(R.string.assign_order_text)+" "+mapDriverList.getName(), AlertStyle.DIALOG);
        alert.addAction(new AlertAction(getString(R.string.yes), AlertActionStyle.DEFAULT, action -> {
            callAssignDriver(mapDriverList.getDriverId());

        }));
        alert.addAction(new AlertAction(getString(R.string.no), AlertActionStyle.DEFAULT, action -> {

        }));


        alert.show((AppCompatActivity) getActivity());

    }

    private void callAssignDriver(Integer driverId) {
        commonHelper.showProgress(getActivity(),getString(R.string.show_prog));
        apiRequestService.callAssignDriver(SECRET_KEY,SharedPreferenceManager.getAuth(getActivity()),SharedPreferenceManager.getOrderId(getActivity()),lang,""+driverId,MapFragment.this);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap == null) {
            fragment.getMapAsync(this);
        }
        if (SharedPreferenceManager.getLanguageCheck(getActivity()).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }




    }

    private void callDriverList() {
        commonHelper.showProgress(getActivity(),getString(R.string.show_prog));
        apiRequestService.callDriverList(SECRET_KEY,SharedPreferenceManager.getAuth(getActivity()),"order_list",lang,MapFragment.this);


    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();


    }


    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof MapDriverResponse) {
            MapDriverResponse driverResponse = (MapDriverResponse) value;
            driverSearch = driverResponse.getDriverSearch();
            if (driverResponse.getStatus() == 1) {
                fillMapData(driverResponse.getDriverSearch());
            }
        }else if(value instanceof LogoutResponse){
            LogoutResponse logoutResponse = (LogoutResponse)value;
            if (logoutResponse.getStatus()==1){
                Toast.makeText(getActivity(),logoutResponse.getMessage(),Toast.LENGTH_LONG).show();
                getActivity().onBackPressed();
            }
        }
    }

    private void fillMapData(List<MapDriverList> data) {
        mMap.clear();
        LatLng new_latlng = new LatLng(Double.valueOf(data.get(0).getLatitude()), Double.valueOf(data.get(0).getLongitude()));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new_latlng, 6);
        mMap.animateCamera(cameraUpdate);
        for (int i = 0; i < data.size(); i++) {
            try {
                listMarker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.valueOf(data.get(i).getLatitude()), Double.valueOf(data.get(i).getLongitude())))
                        .anchor(0.5f, 0.5f)
                        .title(data.get(i).getName())
                        .snippet(data.get(i).getMobile())
                        .icon(BitmapDescriptorFactory.fromBitmap(CommonHelper.createURiMarker(getActivity(), String.valueOf(data.get(i).getImage())))));
            } catch (IOException e) {
                e.printStackTrace();
            }
            mHashMap.put(listMarker, i);
        }

    }

    @Override
    public void onError(Throwable e) {
        commonHelper.hideProgress();
        commonHelper.errorhandling(e,getActivity());
    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }
}
