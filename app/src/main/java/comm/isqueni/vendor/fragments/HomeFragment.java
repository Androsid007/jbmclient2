package comm.isqueni.vendor.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.activity.DriverManagementActivity;
import comm.isqueni.vendor.activity.EmergencyActivity;
import comm.isqueni.vendor.activity.MapsActivity;
import comm.isqueni.vendor.activity.NewOrderManagmentActivity;
import comm.isqueni.vendor.activity.OrderManagementActivity;
import comm.isqueni.vendor.activity.ProducManagementActvivity;
import comm.isqueni.vendor.activity.ShutDownActivity;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.models.HomeResponse;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements Observer {

    LinearLayout shudownLayout, driver_management, orderManagement, productManagement, riskManagement;
    RelativeLayout enable_rl, down_rl;
    TextView totaldriver_tv, totalorder_tv, totalproduct_tv, totalrisk_tv;
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    private String lang;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        init(view);
        onclick();
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(getActivity());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedPreferenceManager.getShopStatus(getContext()) == 1) {
            down_rl.setVisibility(View.VISIBLE);
            enable_rl.setVisibility(View.GONE);
        } else {
            down_rl.setVisibility(View.GONE);
            enable_rl.setVisibility(View.VISIBLE);
        }
        if (SharedPreferenceManager.getLanguageCheck(getActivity()).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }

        callHomeData();
    }

    private void callHomeData() {
        commonHelper.showProgress(getActivity(), getString(R.string.show_prog));
        apiRequestService.callHomeData(SharedPreferenceManager.getAuth(getActivity()),
                SECRET_KEY, "vendor_summary", lang, HomeFragment.this);
    }

    private void onclick() {
        shudownLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dostartActvity(ShutDownActivity.class);
            }
        });
        productManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dostartActvity(ProducManagementActvivity.class);
            }
        });
        riskManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dostartActvity(EmergencyActivity.class);
            }
        });
        orderManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dostartActvity(NewOrderManagmentActivity.class);
            }
        });
        driver_management.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dostartActvity(DriverManagementActivity.class);
            }
        });
    }

    private void dostartActvity(Class<?> activty) {
        startActivity(new Intent(getActivity(), activty));
    }

    private void init(View view) {
        shudownLayout = view.findViewById(R.id.shudownLayout);
        driver_management = view.findViewById(R.id.driver_management);
        orderManagement = view.findViewById(R.id.orderManagement);
        productManagement = view.findViewById(R.id.productManagement);
        riskManagement = view.findViewById(R.id.riskManagement);
        totalrisk_tv = view.findViewById(R.id.totalrisk_tv);
        totaldriver_tv = view.findViewById(R.id.totaldriver_tv);
        totalorder_tv = view.findViewById(R.id.totalorder_tv);
        totalproduct_tv = view.findViewById(R.id.totalproduct_tv);
        down_rl = view.findViewById(R.id.down_rl);
        enable_rl = view.findViewById(R.id.enable_rl);
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof HomeResponse) {
            HomeResponse homeResponse = (HomeResponse) value;
            if (homeResponse.getStatus() == 1) {
                if (homeResponse.getSummary() != null) {
                    if (homeResponse.getSummary().getAllActiveDriver() != null) {
                        totaldriver_tv.setText("Active Drivers :" + homeResponse.getSummary().getAllActiveDriver());
                    }
                    if (homeResponse.getSummary().getNewOrder() != null) {
                        totalorder_tv.setText("New Orders :" + homeResponse.getSummary().getNewOrder());
                    }
                    if (homeResponse.getSummary().getNewOrder() != null) {
                        totalproduct_tv.setText("Total Products :" + homeResponse.getSummary().getAllProduct());
                    }
                    if (homeResponse.getSummary().getNewRisk() != null) {
                        totalrisk_tv.setText("New Risk:" + homeResponse.getSummary().getNewRisk());
                    }

                }
            }

        }

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onComplete() {

    }
}
