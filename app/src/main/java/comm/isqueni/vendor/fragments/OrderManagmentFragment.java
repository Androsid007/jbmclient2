package comm.isqueni.vendor.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import comm.isqueni.vendor.R;
import comm.isqueni.vendor.activity.MapsActivity;
import comm.isqueni.vendor.activity.OrderManagementActivity;
import comm.isqueni.vendor.adapter.OrderManagmentAdapter;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.interf.onRecyclerViewClick;
import comm.isqueni.vendor.models.OrderList;
import comm.isqueni.vendor.models.OrderManagmentResponse;
import comm.isqueni.vendor.network.ApiRequestService;

import static comm.isqueni.vendor.common.CommonStrings.SECRET_KEY;

public class OrderManagmentFragment extends Fragment implements Observer, onRecyclerViewClick {
    RecyclerView order_rv;
    private String lang = "";
    ImageView iv_back;
    OrderManagmentAdapter orderManagmentAdapter;
    List<OrderList> orderLists = new ArrayList<>();
    CommonHelper commonHelper;
    ApiRequestService apiRequestService;
    Boolean aBoolean = false;
    EditText et_serach;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ordermangment_fragmnet, container,false);
        commonHelper = new CommonHelper();
        apiRequestService = new ApiRequestService(getActivity());
        if (SharedPreferenceManager.getLanguageCheck(getActivity()).equals("ar")) {
            lang = "1";
        } else {
            lang = "2";
        }
        init(view);
        onClick();
        return  view;

    }

    @Override
    public void onResume() {
        super.onResume();
        callOrderList();
    }

    private void callOrderList() {
        commonHelper.showProgress(getActivity(), getString(R.string.show_prog));
        apiRequestService.callOrderManagment(SECRET_KEY, SharedPreferenceManager.getAuth(getActivity()), "order_list", lang,OrderManagmentFragment.this);
    }

    private void init(View view) {
        order_rv = view.findViewById(R.id.order_rv);
        et_serach = view.findViewById(R.id.et_serach);
        order_rv.setHasFixedSize(true);
        order_rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    private void onClick() {

        et_serach.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    private void filter(String text) {
        List<OrderList> filteredList = new ArrayList<>();
        for (OrderList row : orderLists) {
            if (row.getProductName() != null)
                if (row.getProductName().toLowerCase().contains(text.toLowerCase())) {
                    filteredList.add(row);
                }
        }
        orderManagmentAdapter.getFilter(filteredList);
    }


    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object value) {
        commonHelper.hideProgress();
        if (value instanceof OrderManagmentResponse) {
            OrderManagmentResponse managmentResponse = (OrderManagmentResponse) value;
            if (managmentResponse.getStatus() == 1) {
                orderLists = managmentResponse.getOrderList();
                if (orderLists != null && orderLists.size() > 0) {
                    orderManagmentAdapter = new OrderManagmentAdapter(orderLists, getActivity(), OrderManagmentFragment.this);
                    order_rv.setAdapter(orderManagmentAdapter);
                    orderManagmentAdapter.notifyDataSetChanged();
                }
            }
            else{
               // Toast.makeText(,"gggggggggg",Toast.LENGTH_LONG).show();

            }
        }


    }

    @Override
    public void onError(Throwable e) {
        Log.d("onError", "onError: ", e);
        commonHelper.hideProgress();
        commonHelper.errorhandling(e, getActivity());
    }

    @Override
    public void onComplete() {
        commonHelper.hideProgress();
    }

    @Override
    public void onRecyClick(int pos) {
        if (orderLists.get(pos).getIsDriverAlreadyAssign() == null) {
            SharedPreferenceManager.setOrderId(getActivity(), String.valueOf(orderLists.get(pos).getOrderId()));
            if (getPermissionCheck()) {
                startActivity(new Intent(getActivity(), MapsActivity.class));
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.already), Toast.LENGTH_LONG).show();
        }
    }

    public boolean getPermissionCheck() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                            aBoolean = true;
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settingsa
                            aBoolean = false;
                            showSettigDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
        return aBoolean;
    }

    private void showSettigDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.ned_permission);
        builder.setMessage(R.string.permission_disclamir);
        builder.setPositiveButton(R.string.got_to_seeting, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
}
