package comm.isqueni.vendor.fragments;


import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import java.util.Locale;

import comm.isqueni.vendor.R;
import comm.isqueni.vendor.activity.HomeActivity;
import comm.isqueni.vendor.common.SharedPreferenceManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    RelativeLayout englishLayout,arabicLayout;
    RadioButton english_rb,arabic_rb;
    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_setting, container, false);
        init(view);
        onClick();
        if (SharedPreferenceManager.getLanguageCheck(getActivity()).equals("ar")){
            arabic_rb.setChecked(true);
        }else {
            english_rb.setChecked(true);
        }
        return view;
    }

    private void onClick() {
        english_rb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    english_rb.setChecked(true);
                    arabic_rb.setChecked(false);
                    SharedPreferenceManager.setLanguageCheck(getActivity(),"en");
                    Locale locale = new Locale("en");
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
                  //  startActivity(new Intent(getActivity(), HomeActivity.class));

                }
            }
        });
        arabic_rb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    arabic_rb.setChecked(true);
                    english_rb.setChecked(false);
                    SharedPreferenceManager.setLanguageCheck(getActivity(),"ar");
                    Locale locale = new Locale("en");
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
                 //   startActivity(new Intent(getActivity(), HomeActivity.class));
                }
            }
        });
        englishLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                english_rb.setChecked(true);
                arabic_rb.setChecked(false);
                SharedPreferenceManager.setLanguageCheck(getActivity(),"en");
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
                startActivity(new Intent(getActivity(), HomeActivity.class));

            }
        });
        arabicLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arabic_rb.setChecked(true);
                english_rb.setChecked(false);
                SharedPreferenceManager.setLanguageCheck(getActivity(),"ar");
                Locale locale = new Locale("ar");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
                startActivity(new Intent(getActivity(), HomeActivity.class));
            }
        });
    }

    private void init(View view) {
        englishLayout = view.findViewById(R.id.englishLayout);
        arabicLayout = view.findViewById(R.id.arabicLayout);

        english_rb =view.findViewById(R.id.english_rb);
        arabic_rb =view.findViewById(R.id.arabic_rb);
    }

}
