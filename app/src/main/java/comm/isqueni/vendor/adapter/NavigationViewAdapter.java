package comm.isqueni.vendor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import comm.isqueni.vendor.R;
import comm.isqueni.vendor.interf.onRecyclerViewClick;
import comm.isqueni.vendor.models.NavigationViewList;

public class NavigationViewAdapter extends RecyclerView.Adapter<NavigationViewAdapter.MyDataViewHolder> {
    List<NavigationViewList> navigationViewList;
    Context context;
    onRecyclerViewClick recyclerViewClick;

    public NavigationViewAdapter(List<NavigationViewList> navigationViewList, Context context, onRecyclerViewClick recyclerViewClick) {
        this.navigationViewList = navigationViewList;
        this.context = context;
        this.recyclerViewClick = recyclerViewClick;
    }

    @NonNull
    @Override
    public MyDataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.navigation_item_layout, viewGroup, false);
        return new MyDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyDataViewHolder myDataViewHolder, int i) {
        myDataViewHolder.itemName.setText(navigationViewList.get(i).getmTitle());
        if (i == 0) {
            myDataViewHolder.iv_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.home));
        } else if (i == 1) {
            myDataViewHolder.iv_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.driver_management));
        }else if (i == 2) {
            myDataViewHolder.iv_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.order_management));
        }else if (i == 3) {
            myDataViewHolder.iv_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.product_management));
        }else if (i == 4) {
            myDataViewHolder.iv_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.risk_management ));
        }else if (i == 5) {
            myDataViewHolder.iv_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.order_management));
        }else if (i == 6) {
            myDataViewHolder.iv_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.logout));
        }else if (i == 7) {
            myDataViewHolder.iv_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.logout));
        }

    }

    @Override
    public int getItemCount() {
        return navigationViewList.size();
    }

    public class MyDataViewHolder extends RecyclerView.ViewHolder {
        TextView itemName;
        LinearLayout linearLayout;
        ImageView iv_icon;

        public MyDataViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.itemName);
            linearLayout = itemView.findViewById(R.id.layout);
            iv_icon = itemView.findViewById(R.id.icon);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerViewClick.onRecyClick(getAdapterPosition());
                }
            });
        }
    }
}
