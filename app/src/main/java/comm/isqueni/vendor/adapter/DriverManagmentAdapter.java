package comm.isqueni.vendor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import comm.isqueni.vendor.R;
import comm.isqueni.vendor.models.DriverSearch;

import static comm.isqueni.vendor.common.CommonStrings.DELETE_DRIVER;

public class DriverManagmentAdapter  extends RecyclerView.Adapter<DriverManagmentAdapter.MyDataViewHolder>{

    List<DriverSearch> driverSearches;
    Context context;
    onRecyclerviewDelete recyclerviewDelete;

    public DriverManagmentAdapter(List<DriverSearch> driverSearches, Context context, onRecyclerviewDelete recyclerviewDelete) {
        this.driverSearches = driverSearches;
        this.context = context;
        this.recyclerviewDelete = recyclerviewDelete;
    }

   public void getFilter(List<DriverSearch> driverSearches){
        this.driverSearches = driverSearches;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View  view = LayoutInflater.from(parent.getContext()).inflate(R.layout.driver_management_card_view,parent,false);
        return new MyDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyDataViewHolder holder, int position) {
        if (driverSearches.get(position).getImage()!=null && !driverSearches.get(position).getImage().equals("")){
            Picasso.with(context).load(driverSearches.get(position).getImage()).placeholder(R.drawable.ic_no_image).resize(1080,720).into(holder.profile_iv);
        }
        if (driverSearches.get(position).getName()!=null && !driverSearches.get(position).getName().equals("")){
            holder.tv_name.setText(driverSearches.get(position).getName());
        }
        if (driverSearches.get(position).getCarType()!=null && !driverSearches.get(position).getCarType().equals("")){
            holder.tv_car_type.setText(driverSearches.get(position).getCarType());
            holder.tv_color.setText(driverSearches.get(position).getCarType());
        }
        if (driverSearches.get(position).getDriverLicenceNumber()!=null && !driverSearches.get(position).getDriverLicenceNumber().equals("")){
            holder.tv_license_no.setText(driverSearches.get(position).getDriverLicenceNumber());
        }
        if (driverSearches.get(position).getMobile()!=null && !driverSearches.get(position).getMobile().equals("")){
            holder.tv_mobile_no.setText(driverSearches.get(position).getMobile());
        }
        if (driverSearches.get(position).getCarNumber()!=null && !driverSearches.get(position).getCarNumber().equals("")){
            holder.tv_car_no.setText(driverSearches.get(position).getCarNumber());
        }
        holder.deleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerviewDelete.onClickDelete(position,DELETE_DRIVER);
            }
        });

    }

    @Override
    public int getItemCount() {
        return driverSearches.size();
    }

    public class MyDataViewHolder extends RecyclerView.ViewHolder{
        TextView tv_name,tv_car_type,tv_color,tv_license_no,tv_mobile_no,tv_car_no;
        ImageView profile_iv;
        RelativeLayout deleteLayout,mainLayout;

        public MyDataViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_car_type = itemView.findViewById(R.id.tv_car_type);
            tv_color = itemView.findViewById(R.id.tv_color);
            tv_license_no = itemView.findViewById(R.id.tv_license_no);
            tv_mobile_no = itemView.findViewById(R.id.tv_mobile_no);
            tv_car_no = itemView.findViewById(R.id.tv_car_no);
            profile_iv = itemView.findViewById(R.id.profile_iv);
            deleteLayout = itemView.findViewById(R.id.deleteLayout);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }

    public interface onRecyclerviewDelete{
        void onClickDelete(int pos ,String key);
    }
}
