package comm.isqueni.vendor.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import comm.isqueni.vendor.R;
import comm.isqueni.vendor.activity.OrderCancelActivity;
import comm.isqueni.vendor.common.SharedPreferenceManager;
import comm.isqueni.vendor.interf.onRecyclerViewClick;
import comm.isqueni.vendor.models.OrderList;
import comm.isqueni.vendor.models.RecentOrderList;

public class RecentOrderManagmneAdapter extends RecyclerView.Adapter<RecentOrderManagmneAdapter.MyViewHolder> {
    List<RecentOrderList> orderLists;
    Context context;
    onRecyclerViewClick recyclerViewClick;

    public RecentOrderManagmneAdapter(List<RecentOrderList> orderLists, Context context, onRecyclerViewClick recyclerViewClick) {
        this.orderLists = orderLists;
        this.context = context;
        this.recyclerViewClick = recyclerViewClick;
    }

    public void getFilter(List<RecentOrderList> orderLists) {
        this.orderLists = orderLists;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_manag_cardview, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (orderLists.get(position).getProductName() != null) {
            holder.tv_name.setText(orderLists.get(position).getProductName());
        }
        if (orderLists.get(position).getQty() != null) {
            holder.qty_tv.setText("" + orderLists.get(position).getQty());
        }
        if (orderLists.get(position).getTotalAmount() != null) {
            holder.total_amt_tv.setText("" + orderLists.get(position).getTotalAmount());
        }
        if (orderLists.get(position).getPaymentMethod() != null) {
            holder.payment_mentod_tv.setText(orderLists.get(position).getPaymentMethod());
        }
        if (orderLists.get(position).getCity() != null && orderLists.get(position).getHouse() != null && orderLists.get(position).getLandmark() != null) {
            holder.address_tv.setText(orderLists.get(position).getCity() + ", " + orderLists.get(position).getHouse() + ", " + orderLists.get(position).getLandmark());
        }
        holder.assign_driver_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewClick.onRecyClick(position);
            }
        });
        if (orderLists.get(position).getIsDriverAlreadyAssign() != null) {
            holder.assign_layout.setVisibility(View.GONE);
            holder.driver_assign_layout.setVisibility(View.VISIBLE);

        } else {
            holder.assign_layout.setVisibility(View.VISIBLE);
            holder.driver_assign_layout.setVisibility(View.GONE);
        }
        if (orderLists.get(position).getMobile() != null && !orderLists.get(position).getMobile().equals("")) {
            holder.driver_phone_tv.setText(orderLists.get(position).getMobile());
        }
       /* if (orderLists.get(position).getD() != null && !orderLists.get(position).getDriverName().equals("")) {
            holder.driver_name_tv.setText(orderLists.get(position).getDriverName());
        }*/
        if (orderLists.get(position).getOrderId() != null) {
            holder.order_no.setText("#" + orderLists.get(position).getOrderId());
        }
        if (orderLists.get(position).getInvoiceNumder() != null) {
            holder.invoice_no.setText("#" + orderLists.get(position).getInvoiceNumder());
        }
        if (orderLists.get(position).getStatus() != null)
            if (orderLists.get(position).getStatus() == 2) {
                //Cancelled
                holder.assign_layout.setVisibility(View.GONE);
                holder.driver_assign_layout.setVisibility(View.VISIBLE);
                holder.order_status.setText(R.string.cancelled);
                holder.driver_assign_layout.setBackground(context.getResources().getDrawable(R.drawable.order_cancelled_bg));
            }

    }

    @Override
    public int getItemCount() {
        return orderLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, qty_tv, order_status, total_amt_tv, payment_mentod_tv, address_tv, driver_name_tv, dateTv, driver_phone_tv, invoice_no, order_no;
        LinearLayout assign_layout;
        RelativeLayout assign_driver_layout, cancel_layout, driver_assign_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            driver_name_tv = itemView.findViewById(R.id.driver_name_tv);
            tv_name = itemView.findViewById(R.id.tv_name);
            driver_phone_tv = itemView.findViewById(R.id.driver_phone_tv);
            qty_tv = itemView.findViewById(R.id.qty_tv);
            total_amt_tv = itemView.findViewById(R.id.total_amt_tv);
            order_status = itemView.findViewById(R.id.order_status);
            payment_mentod_tv = itemView.findViewById(R.id.payment_mentod_tv);
            address_tv = itemView.findViewById(R.id.address_tv);
            dateTv = itemView.findViewById(R.id.dateTv);
            assign_layout = itemView.findViewById(R.id.assign_layout);
            assign_driver_layout = itemView.findViewById(R.id.assign_driver_layout);
            cancel_layout = itemView.findViewById(R.id.cancel_layout);
            invoice_no = itemView.findViewById(R.id.invoice_no);
            order_no = itemView.findViewById(R.id.order_no);
            driver_assign_layout = itemView.findViewById(R.id.driver_assign_layout);
            cancel_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferenceManager.setOrderId(context, "" + orderLists.get(getAdapterPosition()).getOrderId());
                    Intent intent = new Intent(context, OrderCancelActivity.class);
                    context.startActivity(intent);
                }
            });
        }
    }
}
