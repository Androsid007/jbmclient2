package comm.isqueni.vendor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import comm.isqueni.vendor.R;
import comm.isqueni.vendor.interf.onRecyclerViewClick;
import comm.isqueni.vendor.models.CancelOrderList;

public class CancelOrderAdapter  extends RecyclerView.Adapter<CancelOrderAdapter.MyDataViewHolder>{

    List<CancelOrderList> cancelOrderLists;
    Context context;
    onRecyclerViewClick onRecyclerViewClick;
    private int mSelectedItem = -1;

    public CancelOrderAdapter(List<CancelOrderList> cancelOrderLists, Context context, comm.isqueni.vendor.interf.onRecyclerViewClick onRecyclerViewClick) {
        this.cancelOrderLists = cancelOrderLists;
        this.context = context;
        this.onRecyclerViewClick = onRecyclerViewClick;
    }

    @NonNull
    @Override
    public MyDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cancel_order_card_view,parent,false);
        return new MyDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyDataViewHolder holder, int position) {
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*    mCheckedPostion  = position;
                onRecyclerViewClick.onRecyClick(position);
                if (position == mCheckedPostion) {
                    holder.radio_btn.setChecked(false);
                    mCheckedPostion = -1;
                } else {
                    mCheckedPostion = position;
                    notifyDataSetChanged();
                }*/
            }
        });
        if (position == mSelectedItem){
            holder.radio_btn.setChecked(true);
        }else {
            holder.radio_btn.setChecked(false);
        }
        holder.radio_btn.setChecked(position == mSelectedItem);
        holder.textView.setText(cancelOrderLists.get(position).getmString());
        holder.radio_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
        holder.radio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedItem = position;
                onRecyclerViewClick.onRecyClick(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cancelOrderLists.size();
    }

    public class MyDataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;
        RadioButton radio_btn;
        RelativeLayout layout;
        public MyDataViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text);
            radio_btn = itemView.findViewById(R.id.radio_btn);
            layout = itemView.findViewById(R.id.layout);
            layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mSelectedItem = getAdapterPosition();
            notifyItemRangeChanged(0, cancelOrderLists.size());
            onRecyclerViewClick.onRecyClick(getAdapterPosition());
        }
    }
}
