package comm.isqueni.vendor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import comm.isqueni.vendor.R;
import comm.isqueni.vendor.models.OrderHistory;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> {

    List<OrderHistory> orderHistories;
    Context context;

    public OrderHistoryAdapter(List<OrderHistory> orderHistories, Context context) {
        this.orderHistories = orderHistories;
        this.context = context;
    }

    public void getFilter(List<OrderHistory> orderHistories){
        this.orderHistories = orderHistories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_history_card_view,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (orderHistories.get(position).getOrderId()!=null){
            holder.order_id.setText("#"+orderHistories.get(position).getOrderId());
        }
        if (orderHistories.get(position).getProductName()!=null){
            holder.name_tv.setText(orderHistories.get(position).getProductName());
        }
        if (orderHistories.get(position).getQty()!=null){
            holder.qty_tv.setText(""+orderHistories.get(position).getQty());
        }
        if (orderHistories.get(position).getTotalAmount()!=null){
            holder.t_amt_tv.setText(""+orderHistories.get(position).getTotalAmount());
        }
        if (orderHistories.get(position).getPaymentMethod()!=null){
            holder.p_method_tv.setText(orderHistories.get(position).getPaymentMethod());
        }
        if (orderHistories.get(position).getCity()!=null){
            holder.address_tv.setText(orderHistories.get(position).getCity());
        }
        if (orderHistories.get(position).getOrderId()!=null){
            holder.status_tv.setText("In Progress");
        }

    }

    @Override
    public int getItemCount() {
        return orderHistories.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView order_id,name_tv,qty_tv,t_amt_tv,p_method_tv,address_tv,status_tv;
        public MyViewHolder(View itemView) {
            super(itemView);
            status_tv = itemView.findViewById(R.id.status_tv);
            order_id = itemView.findViewById(R.id.order_id);
            name_tv = itemView.findViewById(R.id.name_tv);
            qty_tv = itemView.findViewById(R.id.qty_tv);
            t_amt_tv = itemView.findViewById(R.id.t_amt_tv);
            p_method_tv = itemView.findViewById(R.id.p_method_tv);
            address_tv = itemView.findViewById(R.id.address_tv);

        }
    }
}
