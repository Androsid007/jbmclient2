package comm.isqueni.vendor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import comm.isqueni.vendor.R;
import comm.isqueni.vendor.interf.onRecyclerViewClick;
import comm.isqueni.vendor.models.ProductSearch;

public class ProductManagmentAdapter extends RecyclerView.Adapter<ProductManagmentAdapter.DataViewHolder> implements Filterable {

    List<ProductSearch> searches;
    List<ProductSearch> searchesFiltered;
    Context context;
    onRecyclerViewClick recyclerViewClick;

    public ProductManagmentAdapter(List<ProductSearch> searches, Context context, onRecyclerViewClick recyclerViewClick) {
        this.searches = searches;
        this.context = context;
        this.recyclerViewClick = recyclerViewClick;
    }

    public void filterList(List<ProductSearch> searches) {
        this.searches = searches;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_managment_card_view, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewClick.onRecyClick(position);
            }
        });

        if (searches.get(position).getName()!=null && !searches.get(position).getName().equals("")){
            holder.tv_name.setText(searches.get(position).getName());
        }
        if (searches.get(position).getPhoto()!=null && !searches.get(position).getPhoto().equals("")){
            Picasso.with(context).load(searches.get(position).getPhoto()).centerCrop()
                    .resize(1080,720).placeholder(R.drawable.ic_no_image).into(holder.product_iv);
        }else {
            holder.product_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_no_image));
        }
        if (searches.get(position).getPriceMosque()!=null){
            holder.tv_mosque.setText(""+searches.get(position).getPriceMosque());
        }
        if (searches.get(position).getPriceCorp()!=null){
            holder.tv_corop.setText(""+searches.get(position).getPriceCorp());
        }

    }

    @Override
    public int getItemCount() {
        return searches.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    searchesFiltered = searches;
                } else {
                    List<ProductSearch> filteredList = new ArrayList<>();
                    for (ProductSearch row : searches) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    searchesFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = searchesFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                searchesFiltered = (ArrayList<ProductSearch>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ImageView product_iv;
        TextView tv_name,tv_mosque,tv_corop;
        RelativeLayout mainLayout;
        public DataViewHolder(View itemView) {
            super(itemView);
            product_iv = itemView.findViewById(R.id.product_iv);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_mosque = itemView.findViewById(R.id.tv_mosque);
            tv_corop = itemView.findViewById(R.id.tv_corop);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }

}
