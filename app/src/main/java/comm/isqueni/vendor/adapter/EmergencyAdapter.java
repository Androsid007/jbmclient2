package comm.isqueni.vendor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import comm.isqueni.vendor.R;
import comm.isqueni.vendor.common.CommonHelper;
import comm.isqueni.vendor.models.EmergencyRequestSearch;

import static comm.isqueni.vendor.common.CommonStrings.ACTION_TAKEN;
import static comm.isqueni.vendor.common.CommonStrings.DO_ACTION;

public class EmergencyAdapter extends RecyclerView.Adapter<EmergencyAdapter.MyDataViewHolder> {

    Context context;
    List<EmergencyRequestSearch> emergencyRequestSearches;
    onEmergencyClick emergencyClick;

    public EmergencyAdapter(Context context, List<EmergencyRequestSearch> emergencyRequestSearches, onEmergencyClick emergencyClick) {
        this.context = context;
        this.emergencyRequestSearches = emergencyRequestSearches;
        this.emergencyClick = emergencyClick;
    }
    public void getFilter(List<EmergencyRequestSearch> emergencyRequestSearches){
        this.emergencyRequestSearches = emergencyRequestSearches;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.emergency_adapter_card_view,parent,false);
        return new MyDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyDataViewHolder holder, int position) {

       holder.doactionRl.setOnClickListener(v -> {
           if (emergencyRequestSearches.get(position).getAction()==1){
               emergencyClick.onEmergency(position,DO_ACTION);
           }else {
               emergencyClick.onEmergency(position,ACTION_TAKEN);
           }
       });
        if (emergencyRequestSearches.get(position).getDriverName()!=null
                && !emergencyRequestSearches.get(position).getDriverName().equals("")){
            holder.name_tv.setText(emergencyRequestSearches.get(position).getDriverName());
        }
        if (emergencyRequestSearches.get(position).getAction()==1){
            holder.doactionRl.setBackground(context.getResources().getDrawable(R.drawable.action_taken_bg));
            holder.actionTv.setText(context.getResources().getString(R.string.action_taken));
        }else {
            holder.actionTv.setText(context.getResources().getString(R.string.do_action));
            holder.doactionRl.setBackground(context.getResources().getDrawable(R.drawable.do_action_bg));
        }
        if (emergencyRequestSearches.get(position).getReason()!=null
                && !emergencyRequestSearches.get(position).getReason().equals("")){
            holder.reasonTv.setText(emergencyRequestSearches.get(position).getReason());
        }
        if (emergencyRequestSearches.get(position).getText()!=null
                && !emergencyRequestSearches.get(position).getText().equals("")){
            holder.emergencyTv.setText(emergencyRequestSearches.get(position).getText());
        }
        if (emergencyRequestSearches.get(position).getEmergenyDate()!=null
                && !emergencyRequestSearches.get(position).getEmergenyDate().equals("")){
            holder.dateTv.setText(CommonHelper.getConvertedDate(emergencyRequestSearches.get(position).getEmergenyDate()));
        }



    }


    @Override
    public int getItemCount() {
        return emergencyRequestSearches.size();
    }

    public class MyDataViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout doactionRl;
        TextView name_tv,reasonTv,emergencyTv,dateTv,actionTv;
        public MyDataViewHolder(View itemView) {
            super(itemView);
            dateTv = itemView.findViewById(R.id.dateTv);
            name_tv = itemView.findViewById(R.id.name_tv);
            reasonTv = itemView.findViewById(R.id.reasonTv);
            emergencyTv = itemView.findViewById(R.id.emergencyTv);
            doactionRl = itemView.findViewById(R.id.doactionRl);
            actionTv = itemView.findViewById(R.id.actionTv);
          //  actionTakenRl = itemView.findViewById(R.id.actionTakenRl);
        }
    }

    public interface onEmergencyClick{
        public void onEmergency(int pos,String s);
    }
}
